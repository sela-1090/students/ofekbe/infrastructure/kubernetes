## about the project

this is a main.tf file that deploys a 2 node kuberentes cluster with all the needed namespaces and tools for a ci cd pipeline(argocd, grafan, jenkins, prometheus).

## how to run it
```
download the main.tf
use the az login to ligin to your azure
go to the directory you downloaded it using cd
run terraform init
run terraform apply
```
